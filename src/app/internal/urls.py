from django.urls import path

from app.internal.transport.rest.handlers import InfoMeView

urlpatterns = [
    path("me/", InfoMeView.as_view()),
    path("me/<int:telegram_id>/", InfoMeView.as_view())
]
