# Generated by Django 4.2a1 on 2023-02-13 20:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='external_id',
            new_name='telegram_id',
        ),
        migrations.AddField(
            model_name='user',
            name='phone_number',
            field=models.PositiveIntegerField(default=0, verbose_name='Номер телефона пользователя'),
            preserve_default=False,
        ),
    ]
