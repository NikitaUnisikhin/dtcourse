from django.contrib import admin
from django.db import models

from app.internal.models.user import User


@admin.register(User)
class UserAdmin(models.Model):
    list_display = ('id', 'telegram_id', 'username', 'phone_number')
