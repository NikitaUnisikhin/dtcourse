# DoubleTapp Backend Course

## Запуск проекта:
1) Создайте и заполните файл .env (список необходимых переменных хранится в .env.example)
2) Установите необходиме библиотеки (они находятся в Pipfile)
3) python src/manage.py createsuperuser
4) python src/manage.py runserver
5) python src/manage.py bot

## Комманды:
/start - авторизоваться в боте  
/set_phone - сохранить номер телефона  
/me - получить информацию о себе  
Эндпоинт /me - получить информацию о себе через http запрос  

## Важно: 
В проекте используется python-telegram-bot версии 13.0