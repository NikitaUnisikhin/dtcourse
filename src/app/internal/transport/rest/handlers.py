from typing import Any, Optional

from django.http import HttpRequest, JsonResponse
from django.views import View

from app.internal.services.user_service import get_user


class InfoMeView(View):
    @staticmethod
    def get(request: HttpRequest, telegram_id: Optional[int] = None):
        if telegram_id is not None:
            if not request.user.is_superuser:
                return JsonResponse({"error": {"message": "access denied"}}, status=403)
            return InfoMeView.get_info_about_user("telegram_id", telegram_id)
        return InfoMeView.get_info_about_user("username", request.user.username)

    @staticmethod
    def get_info_about_user(parameter: str, value: Any):
        telegram_user = get_user(parameter, value)
        if telegram_user is None:
            return JsonResponse({"error": {"message": "user is not found"}}, status=404)
        if not telegram_user.phone_number:
            return JsonResponse({"error": {"field": "phone_number", "message": "the phone number is not set"}},
                                status=404)
        return JsonResponse(
            {
                "telegram_id": telegram_user.telegram_id,
                "username": telegram_user.username,
                "phone_number": str(telegram_user.phone_number),
            }
        )
