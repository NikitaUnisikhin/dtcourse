from django.db import models


class User(models.Model):
    telegram_id = models.PositiveIntegerField(
        verbose_name='Id пользователя в телеграмм'
    )
    username = models.TextField(
        verbose_name='Имя пользователя'
    )
    phone_number = models.PositiveIntegerField(
        verbose_name='Номер телефона пользователя',
        null=True
    )

