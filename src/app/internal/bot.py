from telegram.ext import CommandHandler, Dispatcher, Updater
from app.internal.transport.bot.handlers import get_info_me_handler, help_handler, set_phone_handler, start_handler


class Bot:
    def __init__(self, token: str):
        self.updater = Updater(token)
        self.dispatcher: Dispatcher = self.updater.dispatcher

    def configure(self):
        self.dispatcher.add_handler(CommandHandler("start", start_handler))
        self.dispatcher.add_handler(CommandHandler("help", help_handler))
        self.dispatcher.add_handler(CommandHandler("set_phone", set_phone_handler))
        self.dispatcher.add_handler(CommandHandler("me", get_info_me_handler))

    def run(self):
        self.updater.start_polling()
        self.updater.idle()
