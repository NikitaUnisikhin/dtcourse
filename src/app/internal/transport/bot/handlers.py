from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import create_user, save_user_phone_number, get_user
from app.internal.transport.bot.bot_replies import BotReplies


def check_user_saved(update: Update, user):
    if not user:
        update.message.reply_text(BotReplies.USER_NOT_SAVED)
        return False
    return True


def check_phone_number_saved(update: Update, user):
    if not user.phone_number:
        update.message.reply_text(BotReplies.PHONE_NUMBER_NOT_ENTER)
        return False
    return True


def start_handler(update: Update, context: CallbackContext):
    user = update.effective_user
    telegram_id = user.id
    username = user.username
    create_user(telegram_id, username)
    update.message.reply_text(BotReplies.USER_CREATED)


def help_handler(update: Update, context: CallbackContext):
    update.message.reply_text(BotReplies.HELP)


def set_phone_handler(update: Update, context: CallbackContext):
    user = get_user("telegram_id", update.effective_user.id)
    if not check_user_saved(update, user):
        return

    try:
        phone_number = context.args[0]
    except IndexError:
        update.message.reply_text(BotReplies.PHONE_NUMBER_NOT_ENTER)
        return
    save_user_phone_number(update.effective_user.id, phone_number)
    update.message.reply_text(BotReplies.PHONE_NUMBER_SAVED)


def get_info_me_handler(update: Update, context: CallbackContext):
    user = get_user("telegram_id", update.effective_user.id)
    if not check_user_saved(update, user) or not check_phone_number_saved(update, user):
        return

    info = (
        f"Telegram ID: `{user.telegram_id}`\n"
        + f"Username: `{user.username}`\n"
        + f"Phone number: `{user.phone_number}`"
    )
    update.message.reply_text(info, parse_mode="markdown")










