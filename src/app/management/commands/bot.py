from django.core.management import BaseCommand

from app.internal.bot import Bot
from config.settings import TELEGRAM_TOKEN


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        bot = Bot(TELEGRAM_TOKEN)
        bot.configure()
        bot.run()
