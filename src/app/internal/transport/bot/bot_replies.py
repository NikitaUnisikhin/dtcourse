class BotReplies:
    USER_CREATED = "User created.\nUse /set_phone <phone> to set your phone number"
    USER_NOT_SAVED = "Log in to use the command (/start)"
    PHONE_NUMBER_SAVED = "Phone number saved"
    PHONE_NUMBER_NOT_ENTER = "Enter your phone number to use the command (/set_phone <phone>)"
    HELP = (
        "/start - Start using\n"
        + "/set_phone <phone> - Set phone number\n"
        + "/me - Get info about me\n"
        + "/help - Show command list\n"
    )
