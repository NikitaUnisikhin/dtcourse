from app.internal.models.user import User


def create_user(telegram_id, username):
    User.objects.get_or_create(
        telegram_id=telegram_id,
        defaults={"username": username}
    )


def get_user(key, value):
    try:
        user = User.objects.get(**{key: value})
    except User.DoesNotExist:
        return None
    return user


def save_user_phone_number(telegram_id, phone_number):
    user = get_user("telegram_id", telegram_id)
    user.phone_number = phone_number
    user.save()
